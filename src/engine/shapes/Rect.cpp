#include "Rect.h"

namespace eg
{
    Rect::Rect(float x, float y, float width, float height)
        : x(x), y(y), width(width), height(height)
    {
    }

    void Rect::setPosition(float x, float y)
    {
        this->x = x;
        this->y = y;
    }

    void Rect::setSize(float width, float height)
    {
        this->width = width;
        this->height = height;
    }
} // namespace eg
