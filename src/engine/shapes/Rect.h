#pragma once

namespace eg
{
    struct Rect
    {
        float x;
        float y;
        float width;
        float height;

        Rect() = default;
        Rect(float x, float y, float width, float height);

        void setPosition(float x, float y);
        void setSize(float width, float height);
    };
} // namespace eg
