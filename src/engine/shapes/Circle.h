#pragma once

namespace eg
{
    struct Circle
    {
        float x;
        float y;
        float radius;

        Circle() = default;
        Circle(float x, float y, float radius);
        ~Circle();

        void setPosition(float x, float y);
        void setRadius(float radius);
    };
} // namespace eg
