#pragma once
#include <cmath>

namespace eg
{
    struct Vec2
    {
        float x;
        float y;

        Vec2() = default;
        Vec2(float x, float y) : x(x), y(y) {}

        Vec2 operator+(const Vec2 &other) const
        {
            return Vec2(x + other.x, y + other.y);
        }

        Vec2 operator-(const Vec2 &other) const
        {
            return Vec2(x - other.x, y - other.y);
        }

        Vec2 operator*(float scalar) const
        {
            return Vec2(x * scalar, y * scalar);
        }

        float operator*(const Vec2 &other) const
        {
            return x * other.x + y * other.y;
        }

        Vec2 &operator+=(const Vec2 &other)
        {
            x += other.x;
            y += other.y;
            return *this;
        }

        Vec2 &operator+=(Vec2 &&other)
        {
            x += other.x;
            y += other.y;
            return *this;
        }

        void normalize()
        {
            if (abs(x) + abs(y) > 0.0f)
            {
                float length = sqrt(x * x + y * y);
                x /= length;
                y /= length;
            }
        }

        Vec2 normalized() const
        {
            if (abs(x) + abs(y) > 0)
            {
                float length = sqrt(x * x + y * y);
                return Vec2(x / length, y / length);
            }

            return Vec2();
        }

        Vec2 reflect(Vec2 normal) const
        {
            normal.normalize();
            return *this - normal * (*this * normal) * 2;
        }
    };
} // namespace eg
