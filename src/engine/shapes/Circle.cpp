#include "Circle.h"

namespace eg
{
    Circle::Circle(float x, float y, float radius)
        : x(x), y(y), radius(radius)
    {
    }

    Circle::~Circle()
    {
    }

    void Circle::setPosition(float x, float y)
    {
        this->x = x;
        this->y = y;
    }

    void Circle::setRadius(float radius)
    {
        this->radius = radius;
    }
} // namespace eg
