#pragma once
#include "pch.h"
#include "Entity.h"
#include "MyContactListener.h"
#include "Box2DDebugDraw.h"

namespace eg
{
    class Game
    {
    private:
        static ALLEGRO_KEYBOARD_STATE keyboardState;
        static ALLEGRO_KEYBOARD_STATE prevKeyboardState;
        static Game *instance;

        bool hasRemoval;
        int currentEntityIndex;
        bool drawDebug;

        ALLEGRO_TRANSFORM transform;
        std::vector<std::shared_ptr<Entity>> entities;
        std::vector<std::shared_ptr<Entity>> addedEntities;
        std::unordered_map<std::string, std::shared_ptr<Texture>> textureMap;
        std::unique_ptr<b2World> world;
        std::unique_ptr<MyContactListener> myContactListener;
        Box2DDebugDraw box2DDebugDraw;

    public:
        Game();
        ~Game();

        Game(const Game &other) = delete;
        void operator=(const Game &other) = delete;

        void init();
        void render();
        void update(float delta);

        void setRemoval();
        void addEntity(std::shared_ptr<Entity> entity);
        const std::vector<std::shared_ptr<Entity>> &getEntities() const;
        const std::unordered_map<std::string, std::shared_ptr<Texture>> &getTextureMap() const;
        b2World *getWorld();

        static bool isKeyDown(int keycode);
        static bool isKeyJustDown(int keycode);
        static Game *getInstance();
    };

} // namespace eg
