#pragma once
#include "pch.h"
#include "Drawable.h"
#include "Updatable.h"
#include "TextureRegion.h"

namespace eg
{
    class Entity : public Drawable, public Updatable
    {
    protected:
        float x;
        float y;
        bool removal;
        bool canUpdate;
        int id;

    public:
        Entity(float x, float y);
        virtual ~Entity();

        virtual void draw() const override = 0;
        virtual void update(float delta) override = 0;

        bool shouldRemove() const;
        void remove();

        void setId(int id);
        int getId() const;

        virtual b2Vec2 getPosition() const;
    };

} // namespace eg
