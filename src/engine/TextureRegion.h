#pragma once
#include "pch.h"
#include "Drawable.h"
#include "shapes/Rect.h"
#include "Texture.h"

namespace eg
{
    class TextureRegion : public Drawable
    {
    protected:
        float x;
        float y;
        Rect region;
        std::shared_ptr<Texture> texture;

    public:
        TextureRegion(std::shared_ptr<Texture> texture);
        TextureRegion(std::shared_ptr<Texture> texture, float x, float y);
        TextureRegion(std::shared_ptr<Texture> texture, float x, float y, Rect region);
        TextureRegion(std::shared_ptr<Texture> texture, Rect region);
        ~TextureRegion();

        virtual void setPosition(float x, float y);
        virtual void setRegion(const Rect &region);

        virtual void draw() const override;
    };
} // namespace eg
