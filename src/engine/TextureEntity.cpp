#include "TextureEntity.h"

namespace eg
{
    TextureEntity::TextureEntity(TextureRegion textureRegion, float x, float y)
        : Entity(x, y), textureRegion(textureRegion)
    {
    }

    TextureEntity::~TextureEntity()
    {
    }

    void TextureEntity::draw() const
    {
        textureRegion.draw();
    }

    void TextureEntity::update(float delta)
    {
        textureRegion.setPosition(x, y);
    }

} // namespace eg
