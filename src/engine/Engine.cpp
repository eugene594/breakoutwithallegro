#include "Engine.h"
#include "Game.h"

namespace eg
{
    Engine::Engine(const std::string &title, int width, int height)
        : title(title), screenWidth(width), screenHeight(height)
    {
    }

    Engine::~Engine()
    {
    }

    bool Engine::init()
    {
        if (!al_init())
        {
            std::cerr << "Unable to init Allegro." << std::endl;
            return false;
        }

        if (!al_init_image_addon())
        {
            std::cerr << "Unable to init image addon." << std::endl;
            return false;
        }

        if (!al_init_primitives_addon())
        {
            std::cerr << "Unable to init primitives addon." << std::endl;
            return false;
        }

        if (!al_install_keyboard())
        {
            std::cerr << "Unable to install keyboard." << std::endl;
            return false;
        }

        return true;
    }

    void Engine::run()
    {
        if (!init())
        {
            return;
        }

        // Enable vertical sync.
        al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_REQUIRE);
        ALLEGRO_DISPLAY *display = al_create_display(screenWidth, screenHeight);
        ALLEGRO_EVENT_QUEUE *eventQueue = al_create_event_queue();

        al_register_event_source(eventQueue, al_get_keyboard_event_source());
        al_register_event_source(eventQueue, al_get_display_event_source(display));

        Game game;
        game.init();

        float delta = 0;
        double lastTime = al_get_time();

        bool running = true;
        while (running)
        {
            while (!al_is_event_queue_empty(eventQueue))
            {
                ALLEGRO_EVENT event;
                al_wait_for_event(eventQueue, &event);

                switch (event.type)
                {
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    running = false;
                    break;
                }
            }

            double currentTime = al_get_time();
            delta = currentTime - lastTime;
            lastTime = currentTime;

            game.update(delta);
            game.render();
        }

        al_destroy_event_queue(eventQueue);
        al_destroy_display(display);
    }
} // namespace eg
