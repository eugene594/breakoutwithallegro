#include "Entity.h"
#include "Game.h"

namespace eg
{
    Entity::Entity(float x, float y)
        : x(x), y(y), canUpdate(true), removal(false)
    {
    }

    Entity::~Entity()
    {
    }

    bool Entity::shouldRemove() const
    {
        return removal;
    }

    void Entity::remove()
    {
        removal = true;
        Game::getInstance()->setRemoval();
    }

    void Entity::setId(int id)
    {
        this->id = id;
    }

    int Entity::getId() const
    {
        return id;
    }

    b2Vec2 Entity::getPosition() const
    {
        return b2Vec2(x, y);
    }
} // namespace eg
