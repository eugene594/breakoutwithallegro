#include "Block.h"

namespace eg
{
    Block::Block(b2World *world, TextureRegion textureRegion, float x, float y)
        : TextureEntity(textureRegion, x, y), world(world)
    {
        constexpr float width = 16;
        constexpr float height = 8;

        b2BodyDef bodyDef;
        bodyDef.type = b2BodyType::b2_staticBody;
        bodyDef.position.Set(x + width / 2, y + height / 2);
        body = world->CreateBody(&bodyDef);
        b2PolygonShape shape;
        shape.SetAsBox(width / 2, height / 2);
        body->CreateFixture(&shape, 0);
        body->SetUserData(this);
    }

    Block::~Block()
    {
    }

    void Block::update(float delta)
    {
        if (removal)
        {
            destroy();
        }
        TextureEntity::update(delta);
    }

    void Block::destroy()
    {
        remove();
        world->DestroyBody(body);
    }

    void Block::hitByBall()
    {
        removal = true;
    }

} // namespace eg
