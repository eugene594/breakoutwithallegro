#include "BallVanishEffect.h"

namespace eg
{
    BallVanishEffect::BallVanishEffect(TextureRegion textureRegion, float x, float y, b2Vec2 velocity)
        : TextureEntity(textureRegion, x, y), velocity(velocity), index(0), elapsed(0)
    {
        regions.emplace_back(0, 0, 8, 8);
        regions.emplace_back(8, 0, 8, 8);
        regions.emplace_back(16, 0, 8, 8);

        textureRegion.setRegion(regions[index]);
    }

    BallVanishEffect::~BallVanishEffect()
    {
    }

    void BallVanishEffect::update(float delta)
    {
        constexpr float frameDuration = 1.0f / 16.0f;
        elapsed += delta;
        if (elapsed >= frameDuration)
        {
            elapsed = 0;
            index++;

            if (index < regions.size())
            {
                textureRegion.setRegion(regions[index]);
            }
            else
            {
                remove();
            }
        }

        x += velocity.x * delta;
        y += velocity.y * delta;

        TextureEntity::update(delta);
    }

} // namespace eg
