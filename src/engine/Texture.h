#pragma once
#include "pch.h"

namespace eg
{
    class Texture
    {
    private:
        ALLEGRO_BITMAP *bitmap;

    public:
        Texture(const std::string &filePath);
        ~Texture();

        Texture(const Texture &other) = delete;
        Texture &operator=(const Texture &other) = delete;

        Texture(Texture &&other);
        Texture &operator=(Texture &&other);

        ALLEGRO_BITMAP *getBitmap() const;
    };
} // namespace eg
