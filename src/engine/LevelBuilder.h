#pragma once
#include "pch.h"
#include "Texture.h"
#include "Game.h"

namespace eg
{
    class LevelBuilder
    {
    private:
        std::shared_ptr<Texture> blockTexture;
        Game *game;
        b2World *world;

        void createBounds();

    public:
        LevelBuilder(Game *game, std::shared_ptr<Texture> blockTexture);
        ~LevelBuilder();

        void makeLevel(int level);
    };

} // namespace eg
