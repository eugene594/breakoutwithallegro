#pragma once

namespace eg
{
    class Drawable
    {
    public:
        virtual void draw() const = 0;
    };
} // namespace eg
