#pragma once
#include "pch.h"
#include "Entity.h"

namespace eg
{
    class Engine
    {
    private:
        std::string title;
        int screenWidth;
        int screenHeight;

        bool init();

    public:
        Engine(const std::string &title, int width, int height);
        ~Engine();

        void run();
    };
} // namespace eg
