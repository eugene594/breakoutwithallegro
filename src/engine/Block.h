#pragma once
#include "pch.h"
#include "TextureEntity.h"

namespace eg
{
    class Block : public TextureEntity
    {
    private:
        b2World *world;
        b2Body *body;
        void destroy();

    public:
        Block(b2World *world, TextureRegion textureRegion, float x, float y);
        ~Block();

        virtual void update(float delta) override;
        void hitByBall();
    };

} // namespace eg
