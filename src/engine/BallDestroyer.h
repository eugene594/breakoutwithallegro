#pragma once
#include "pch.h"
#include "Entity.h"

namespace eg
{
    class BallDestroyer : public Entity
    {
    public:
        BallDestroyer(float x, float y, float width, float height);
        ~BallDestroyer();

        virtual void update(float delta) override;
        virtual void draw() const override;
    };

} // namespace eg
