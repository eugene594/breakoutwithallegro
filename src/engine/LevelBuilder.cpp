#include "LevelBuilder.h"
#include "Game.h"
#include "TextureRegion.h"
#include "Block.h"
#include "BallDestroyer.h"

namespace eg
{
    LevelBuilder::LevelBuilder(Game *game, std::shared_ptr<Texture> blockTexture)
        : game(game), blockTexture(blockTexture), world(game->getWorld())
    {
    }

    LevelBuilder::~LevelBuilder()
    {
    }

    void LevelBuilder::createBounds()
    {
        // Create ceiling
        b2BodyDef ceilingBodyDef;
        ceilingBodyDef.type = b2BodyType::b2_staticBody;
        ceilingBodyDef.position.Set(72.0f, 4.0f);
        b2Body *ceilingBody = world->CreateBody(&ceilingBodyDef);
        b2PolygonShape ceilingBox;
        ceilingBox.SetAsBox(72.0f, 4.0f);
        ceilingBody->CreateFixture(&ceilingBox, 0);

        // Create left wall
        b2BodyDef leftWallBodyDef;
        leftWallBodyDef.type = b2BodyType::b2_staticBody;
        leftWallBodyDef.position.Set(4.0f, 72.0f);
        b2Body *leftWallBody = world->CreateBody(&leftWallBodyDef);
        b2PolygonShape leftWallBox;
        leftWallBox.SetAsBox(4.0f, 72.0f);
        leftWallBody->CreateFixture(&leftWallBox, 0);

        b2BodyDef leftWallBlockBodyDef;
        leftWallBlockBodyDef.type = b2BodyType::b2_staticBody;
        leftWallBlockBodyDef.position.Set(8.0f, 120.0f);
        b2Body *leftWallBlockBody = world->CreateBody(&leftWallBlockBodyDef);
        b2PolygonShape leftWallBlockBox;
        leftWallBlockBox.SetAsBox(8.0f, 8.0f);
        leftWallBlockBody->CreateFixture(&leftWallBlockBox, 0);

        // Create right wall
        b2BodyDef rightWallBodyDef;
        rightWallBodyDef.type = b2BodyType::b2_staticBody;
        rightWallBodyDef.position.Set(140.0f, 72.0f);
        b2Body *rightWallBody = world->CreateBody(&rightWallBodyDef);
        b2PolygonShape rightWallBox;
        rightWallBox.SetAsBox(4.0f, 72.0f);
        rightWallBody->CreateFixture(&rightWallBox, 0);

        b2BodyDef rightWallBlockBodyDef;
        rightWallBlockBodyDef.type = b2BodyType::b2_staticBody;
        rightWallBlockBodyDef.position.Set(136.0f, 120.0f);
        b2Body *rightWallBlockBody = world->CreateBody(&rightWallBlockBodyDef);
        b2PolygonShape rightWallBlockBox;
        rightWallBlockBox.SetAsBox(8.0f, 8.0f);
        rightWallBlockBody->CreateFixture(&rightWallBlockBox, 0);

        // Create bottom wall (BallDestroyer)
        std::shared_ptr<BallDestroyer> ballDestroyer = std::make_shared<BallDestroyer>(72.0f, 140.0f, 128.0f, 8.0f);
        game->addEntity(ballDestroyer);
        /*
        b2BodyDef bottomWallBodyDef;
        bottomWallBodyDef.type = b2BodyType::b2_staticBody;
        bottomWallBodyDef.position.Set(72.0f, 140.0f);
        bottomWallBodyDef.userData = "destroyer";
        b2Body *bottomWallBody = world->CreateBody(&bottomWallBodyDef);
        b2PolygonShape bottomWallBox;
        bottomWallBox.SetAsBox(64.0f, 4.0f);
        bottomWallBody->CreateFixture(&bottomWallBox, 0);
        */
    }

    void LevelBuilder::makeLevel(int level)
    {
        createBounds();

        TextureRegion region = TextureRegion(blockTexture, Rect{0, 8, 16, 8});
        TextureRegion regions[] = {
            TextureRegion(blockTexture, Rect{16, 0, 16, 8}),
            TextureRegion(blockTexture, Rect{0, 8, 16, 8}),
            TextureRegion(blockTexture, Rect{16, 8, 16, 8}),
            TextureRegion(blockTexture, Rect{24, 0, 16, 8}),
        };

        for (size_t i = 0; i < 8; i++)
        {
            for (size_t j = 0; j < 5; j++)
            {
                game->addEntity(std::make_shared<Block>(world, region, 8 + i * 16, 8 + j * 8));
            }
        }
    }
} // namespace eg
