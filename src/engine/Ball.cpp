#include "Ball.h"
#include "BallVanishEffect.h"
#include "Block.h"
#include "Board.h"
#include "Game.h"

namespace eg
{

    Ball::Ball(b2World *world, TextureRegion textureRegion, float x, float y)
        : TextureEntity(textureRegion, x, y), world(world), speed(120), radius(4)
    {
        createBody(0.2f, -1.0f);
    }

    Ball::Ball(b2World *world, TextureRegion TextureRegion, float x, float y, float dirX, float dirY, float speed)
        : TextureEntity(textureRegion, x, y), world(world), speed(speed), radius(4)
    {
        createBody(dirX, dirY);
    }

    Ball::~Ball()
    {
    }

    void Ball::createBody(float dirX, float dirY)
    {
        b2Vec2 dir(dirX, dirY);
        dir.Normalize();

        b2BodyDef bodyDef;
        bodyDef.position.Set(x + radius, y + radius);
        bodyDef.type = b2BodyType::b2_dynamicBody;
        bodyDef.fixedRotation = true;
        bodyDef.userData = this;
        bodyDef.linearVelocity.Set(speed * dir.x, speed * dir.y);

        b2CircleShape circleShape;
        circleShape.m_radius = radius;
        body = world->CreateBody(&bodyDef);
        b2Fixture *fixture = body->CreateFixture(&circleShape, 1.0f);
        fixture->SetFriction(0.0f);
        fixture->SetRestitution(1.0f);
    }

    void Ball::update(float delta)
    {
        if (!canUpdate)
        {
            return;
        }

        b2Vec2 position = body->GetPosition();
        x = position.x - radius;
        y = position.y - radius;

        if (removal)
        {
            destroy();
        }

        TextureEntity::update(delta);
    }

    b2Vec2 Ball::getPosition() const
    {
        return b2Vec2(x + radius, y + radius);
    }

    void Ball::setSpeed(float speed)
    {
        this->speed = speed;
    }

    void Ball::destroy()
    {
        // Create vanish effect.
        world->DestroyBody(body);
        remove();
    }

    void Ball::hitDestroyer()
    {
        removal = true;
        b2Vec2 velocity = body->GetLinearVelocity();
        velocity *= 0.5f;
        std::shared_ptr<BallVanishEffect> ballVanishEffect = std::make_shared<BallVanishEffect>(textureRegion, x, y, velocity);
        Game::getInstance()->addEntity(ballVanishEffect);
    }

    void Ball::hitBoard(b2Vec2 ratio)
    {
        b2Vec2 velocity = body->GetLinearVelocity();
        velocity.x = ratio.x * speed;
        velocity.Normalize();
        velocity.x *= speed;
        velocity.y *= speed;
        body->SetLinearVelocity(velocity);
    }
} // namespace eg
