#include "Board.h"
#include "Ball.h"

namespace eg
{
    Board::Board(Game *game, TextureRegion textureRegion, float x, float y)
        : TextureEntity(textureRegion, x, y), game(game), speed(80), width(48), height(16), shootTimeLeft(0)
    {
        b2BodyDef bodyDef;
        bodyDef.type = b2BodyType::b2_kinematicBody;
        bodyDef.position.Set(x + width / 2.0f, y + height / 2.0f);

        b2PolygonShape shape;
        shape.SetAsBox(width / 2.0f, height / 2.0f);
        body = game->getWorld()->CreateBody(&bodyDef);
        body->CreateFixture(&shape, 0);
        body->SetUserData(this);
    }

    Board::~Board()
    {
    }

    void Board::update(float delta)
    {
        shootTimeLeft -= delta;

        if (Game::isKeyDown(ALLEGRO_KEY_LEFT) || Game::isKeyDown(ALLEGRO_KEY_A))
        {
            x -= speed * delta;

            x = std::max(16.0f, x);
        }

        if (Game::isKeyDown(ALLEGRO_KEY_RIGHT) || Game::isKeyDown(ALLEGRO_KEY_D))
        {
            x += speed * delta;
            x = std::min(128.0f - width, x);
        }

        body->SetTransform(b2Vec2(x + width / 2.0f, y + height / 2.0f), 0.0f);

        if (Game::isKeyJustDown(ALLEGRO_KEY_SPACE))
        {
            shoot();
        }

        if (Game::isKeyDown(ALLEGRO_KEY_UP))
        {
            shoot();
        }

        TextureEntity::update(delta);
    }

    void Board::shoot()
    {
        if (shootTimeLeft > 0)
        {
            return;
        }

        shootTimeLeft = 0.1f;

        std::unordered_map<std::string, std::shared_ptr<Texture>> textureMap = game->getTextureMap();
        std::shared_ptr<Texture> ballTexture = textureMap["assets/ball.png"];
        TextureRegion ballTextureRegion = TextureRegion(ballTexture, Rect(24, 0, 8, 8));
        game->addEntity(std::make_shared<Ball>(game->getWorld(), ballTextureRegion, x + 24, y - 8));
    }

    b2Vec2 Board::getPositionRatio(b2Vec2 position) const
    {
        float halfWidth = width / 2.0f;
        float halfHeight = height / 2.0f;
        float centerX = x + halfWidth;
        float centerY = y + halfHeight;

        float ratioX = (position.x - centerX) / halfWidth;
        float ratioY = (position.y - centerY) / halfHeight;

        return b2Vec2(ratioX, ratioY);
    }
} // namespace eg
