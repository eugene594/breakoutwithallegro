#include "BallDestroyer.h"
#include "Game.h"

namespace eg
{
    BallDestroyer::BallDestroyer(float x, float y, float width, float height)
        : Entity(x, y)
    {
        b2BodyDef bodyDef;
        bodyDef.type = b2BodyType::b2_staticBody;
        bodyDef.position.Set(x, y);
        b2World *world = Game::getInstance()->getWorld();
        b2Body *body = world->CreateBody(&bodyDef);
        body->SetUserData(this);
        b2PolygonShape shape;
        shape.SetAsBox(width / 2, height / 2);
        body->CreateFixture(&shape, 0);
    }

    BallDestroyer::~BallDestroyer()
    {
    }

    void BallDestroyer::draw() const
    {
    }

    void BallDestroyer::update(float delta)
    {
    }
} // namespace eg
