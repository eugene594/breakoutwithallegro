#pragma once
#include "pch.h"

namespace eg
{
    class MyContactListener : public b2ContactListener
    {
    public:
        MyContactListener();
        ~MyContactListener();

        void PreSolve(b2Contact *contact, const b2Manifold *oldManifold) override;
    };

} // namespace eg
