#include "Texture.h"

namespace eg
{
    Texture::Texture(const std::string &filePath)
    {
        bitmap = al_load_bitmap(filePath.c_str());
    }

    Texture::~Texture()
    {
        if (bitmap)
        {
            al_destroy_bitmap(bitmap);
        }
    }

    Texture::Texture(Texture &&other)
        : bitmap(other.bitmap)
    {
        other.bitmap = nullptr;
    }

    Texture &Texture::operator=(Texture &&other)
    {
        bitmap = other.bitmap;
        other.bitmap = nullptr;
        return *this;
    }

    ALLEGRO_BITMAP *Texture::getBitmap() const
    {
        return bitmap;
    }
} // namespace eg
