#pragma once
#include "pch.h"
#include "TextureEntity.h"

namespace eg
{
    class Ball : public TextureEntity
    {
    private:
        b2Body *body;
        float radius;
        float speed;
        b2World *world;

        void createBody(float dirX, float dirY);
        void destroy();

    public:
        Ball(b2World *world, TextureRegion textureRegion, float x, float y);
        Ball(b2World *world, TextureRegion textureRegion, float x, float y, float dirX, float dirY, float speed);
        ~Ball();

        virtual void update(float delta) override;
        b2Vec2 getPosition() const override;
        void setSpeed(float speed);
        void hitDestroyer();
        void hitBoard(b2Vec2 ratio);
    };

} // namespace eg