#include "Sprite.h"

namespace eg
{
    Sprite::Sprite(std::shared_ptr<Texture> texture)
        : TextureRegion(texture, 0, 0)
    {
    }

    Sprite::Sprite(std::shared_ptr<Texture> texture, float x, float y)
        : TextureRegion(texture, x, y)
    {
    }

    Sprite::Sprite(std::shared_ptr<Texture> texture, float x, float y, Rect region)
        : TextureRegion(texture, x, y, region)
    {
    }
} // namespace eg
