#include "TextureRegion.h"

namespace eg
{
    TextureRegion::TextureRegion(std::shared_ptr<Texture> texture)
        : TextureRegion(texture, 0, 0)
    {
    }

    TextureRegion::TextureRegion(std::shared_ptr<Texture> texture, Rect region)
        : TextureRegion(texture, 0, 0, region)
    {
    }

    TextureRegion::TextureRegion(std::shared_ptr<Texture> texture, float x, float y)
        : x(x), y(y), texture(texture), region(Rect{0, 0, static_cast<float>(al_get_bitmap_width(texture->getBitmap())), static_cast<float>(al_get_bitmap_height(texture->getBitmap()))})
    {
    }

    TextureRegion::TextureRegion(std::shared_ptr<Texture> texture, float x, float y, Rect region)
        : x(x), y(y), region(region), texture(texture)
    {
    }

    TextureRegion::~TextureRegion()
    {
    }

    void TextureRegion::setPosition(float x, float y)
    {
        this->x = x;
        this->y = y;
    }

    void TextureRegion::setRegion(const Rect &rect)
    {
        region = rect;
    }

    void TextureRegion::draw() const
    {
        al_draw_bitmap_region(texture->getBitmap(), region.x, region.y, region.width, region.height, x, y, 0);
    }
} // namespace eg
