#pragma once
#include "Entity.h"

namespace eg
{
    class TextureEntity : public Entity
    {
    protected:
        TextureRegion textureRegion;

    public:
        TextureEntity(TextureRegion textureRegion, float x, float y);
        virtual ~TextureEntity();

        virtual void draw() const override;
        virtual void update(float delta) override;
    };

} // namespace eg
