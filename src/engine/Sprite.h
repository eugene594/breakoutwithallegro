#pragma once
#include "pch.h"
#include "TextureRegion.h"
#include "shapes/Rect.h"
#include "Updatable.h"

namespace eg
{
    class Sprite : public TextureRegion
    {
    public:
        Sprite(std::shared_ptr<Texture> texture);
        Sprite(std::shared_ptr<Texture> texture, float x, float y);
        Sprite(std::shared_ptr<Texture> texture, float x, float y, Rect region);
    };
} // namespace eg
