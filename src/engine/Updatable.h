#pragma once

namespace eg
{
    class Updatable
    {
    public:
        virtual void update(float delta) = 0;
    };
} // namespace eg