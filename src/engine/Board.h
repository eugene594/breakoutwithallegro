#pragma once
#include "pch.h"
#include "TextureEntity.h"
#include "Game.h"

namespace eg
{
    class Board : public TextureEntity
    {
    private:
        float speed;
        float width;
        float height;
        Game *game;
        b2Body *body;
        float shootTimeLeft;
        void shoot();

    public:
        Board(Game *game, TextureRegion textureRegion, float x, float y);
        ~Board();
        virtual void update(float delta) override;

        b2Vec2 getPositionRatio(b2Vec2 position) const;
    };

} // namespace eg
