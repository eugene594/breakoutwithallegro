#include "MyContactListener.h"
#include "Ball.h"
#include "Block.h"
#include "Board.h"
#include "BallDestroyer.h"

namespace eg
{
    MyContactListener::MyContactListener()
    {
    }

    MyContactListener::~MyContactListener()
    {
    }

    void MyContactListener::PreSolve(b2Contact *contact, const b2Manifold *oldManifold)
    {
        const b2Body *bodyA = contact->GetFixtureA()->GetBody();
        const b2Body *bodyB = contact->GetFixtureB()->GetBody();

        Entity *entityA = static_cast<Entity *>(bodyA->GetUserData());
        Entity *entityB = static_cast<Entity *>(bodyB->GetUserData());

        // Check BodyA
        if (Ball *ball = dynamic_cast<Ball *>(entityA))
        {
            if (Block *block = dynamic_cast<Block *>(entityB))
            {
                block->hitByBall();
            }
            else if (BallDestroyer *ballDestroyer = dynamic_cast<BallDestroyer *>(entityB))
            {
                ball->hitDestroyer();
            }
            else if (Board *board = dynamic_cast<Board *>(entityB))
            {
                b2Vec2 ratio = board->getPositionRatio(ball->getPosition());
                ball->hitBoard(ratio);
            }
        }

        // Check BodyB
        if (Ball *ball = dynamic_cast<Ball *>(entityB))
        {
            if (Block *block = dynamic_cast<Block *>(entityA))
            {
                block->hitByBall();
            }
            else if (BallDestroyer *ballDestroyer = dynamic_cast<BallDestroyer *>(entityA))
            {
                ball->hitDestroyer();
            }
            else if (Board *board = dynamic_cast<Board *>(entityA))
            {
                b2Vec2 ratio = board->getPositionRatio(ball->getPosition());
                ball->hitBoard(ratio);
            }
        }
    }

} // namespace eg
