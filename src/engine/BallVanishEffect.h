#pragma once
#include "pch.h"
#include "TextureEntity.h"

namespace eg
{
    class BallVanishEffect : public TextureEntity
    {
    private:
        int index;
        float elapsed;
        b2Vec2 velocity;
        std::vector<Rect> regions;

    public:
        BallVanishEffect(TextureRegion textureRegion, float x, float y, b2Vec2 velocity);
        ~BallVanishEffect();

        virtual void update(float delta) override;
    };

} // namespace eg
