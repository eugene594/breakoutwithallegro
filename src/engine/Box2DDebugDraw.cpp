#include "Box2DDebugDraw.h"

namespace eg
{
    Box2DDebugDraw::Box2DDebugDraw()
    {
        m_drawFlags = e_shapeBit /* | e_jointBit | e_aabbBit | e_pairBit | e_centerOfMassBit */;
    }

    Box2DDebugDraw::~Box2DDebugDraw()
    {
    }

    void Box2DDebugDraw::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
    {
        std::vector<float> v;
        v.reserve(vertexCount * 2);
        for (int i = 0; i < vertexCount; i++)
        {
            v.push_back(vertices[i].x);
            v.push_back(vertices[i].y);
        }

        al_draw_polygon(&v[0], vertexCount, ALLEGRO_LINE_JOIN::ALLEGRO_LINE_JOIN_NONE, al_map_rgba_f(color.r, color.g, color.b, color.a), 1.0f, 0.0f);
    }

    void Box2DDebugDraw::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
    {
        std::vector<float> v;
        v.reserve(vertexCount * 2);
        for (int i = 0; i < vertexCount; i++)
        {
            v.push_back(vertices[i].x);
            v.push_back(vertices[i].y);
        }

        // al_draw_filled_polygon(&v[0], vertexCount, al_map_rgba_f(color.r, color.g, color.b, color.a));
        al_draw_polygon(&v[0], vertexCount, ALLEGRO_LINE_JOIN::ALLEGRO_LINE_JOIN_NONE, al_map_rgba_f(color.r, color.g, color.b, color.a), 1.0f, 0.0f);
    }

    void Box2DDebugDraw::DrawCircle(const b2Vec2 &center, float radius, const b2Color &color)
    {
        al_draw_circle(center.x, center.y, radius, al_map_rgba_f(color.r, color.g, color.b, color.a), 1.0f);
    }

    void Box2DDebugDraw::DrawSolidCircle(const b2Vec2 &center, float radius, const b2Vec2 &axis, const b2Color &color)
    {
        al_draw_circle(center.x, center.y, radius, al_map_rgba_f(color.r, color.g, color.b, color.a), 1.0f);
        // al_draw_filled_circle(center.x, center.y, radius, al_map_rgba_f(color.r, color.g, color.b, color.a));
    }

    void Box2DDebugDraw::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color)
    {
        al_draw_line(p1.x, p1.y, p2.x, p2.y, al_map_rgba_f(color.r, color.g, color.b, color.a), 1.0f);
    }

    void Box2DDebugDraw::DrawTransform(const b2Transform &xf)
    {
        float originX = xf.p.x;
        float originY = xf.p.y;
        al_draw_pixel(originX, originY, al_map_rgb_f(0.0f, 0.0f, 0.75f));

        b2Vec2 yAxis = xf.q.GetYAxis();
        al_draw_line(originX, originY, originX + yAxis.x, originY + yAxis.y, al_map_rgb_f(0.75f, 0.0f, 0.0f), 1.0f);

        b2Vec2 xAxis = xf.q.GetXAxis();
        al_draw_line(originX, originY, originX + xAxis.x, originY + xAxis.y, al_map_rgb_f(0.0f, 0.75f, 0.0f), 1.0f);
    }

    void Box2DDebugDraw::DrawPoint(const b2Vec2 &p, float size, const b2Color &color)
    {
        al_draw_filled_circle(p.x, p.y, size / 2.0f, al_map_rgba_f(color.r, color.g, color.b, color.a));
    }
} // namespace eg
