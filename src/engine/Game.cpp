#include "Game.h"
#include "Board.h"
#include "Ball.h"
#include "LevelBuilder.h"

namespace eg
{
    /* static definitions */
    ALLEGRO_KEYBOARD_STATE Game::keyboardState;
    ALLEGRO_KEYBOARD_STATE Game::prevKeyboardState;
    Game *Game::instance;

    Game::Game()
        : hasRemoval(false), currentEntityIndex(0), box2DDebugDraw(), drawDebug(false)
    {
        instance = this;
        world = std::make_unique<b2World>(b2Vec2(0, 0));
        world->SetDebugDraw(&box2DDebugDraw);

        myContactListener = std::make_unique<MyContactListener>();
        world->SetContactListener(myContactListener.get());
    }

    Game::~Game()
    {
    }

    void Game::init()
    {
        std::shared_ptr<Texture>
            backgroundTexture = std::make_shared<Texture>("assets/background.png");
        std::shared_ptr<Texture> blocksTexture = std::make_shared<Texture>("assets/blocks.png");
        std::shared_ptr<Texture> boardTexture = std::make_shared<Texture>("assets/board.png");
        std::shared_ptr<Texture> ballTexture = std::make_shared<Texture>("assets/ball.png");

        textureMap.emplace("assets/background.png", backgroundTexture);
        textureMap.emplace("assets/blocks.png", blocksTexture);
        textureMap.emplace("assets/board.png", boardTexture);
        textureMap.emplace("assets/ball.png", ballTexture);

        entities.emplace_back(std::make_shared<TextureEntity>(TextureRegion(backgroundTexture), 0, 0));
        entities.emplace_back(std::make_shared<Board>(this, boardTexture, 48, 112));

        LevelBuilder levelBuilder(this, blocksTexture);
        levelBuilder.makeLevel(1);

        al_build_transform(&transform, 0, 0, 5, 5, 0);
    }

    void Game::render()
    {
        al_clear_to_color(al_map_rgb(0, 0, 0));
        al_use_transform(&transform);
        for (auto &entity : entities)
        {
            entity->draw();
        }

        if (drawDebug)
        {
            world->DebugDraw();
        }
        al_flip_display();
    }

    void Game::update(float delta)
    {
        prevKeyboardState = keyboardState;
        al_get_keyboard_state(&keyboardState);

        if (isKeyJustDown(ALLEGRO_KEY_B))
        {
            drawDebug = !drawDebug;
        }

        constexpr float timeStep = 1.0f / 60.0f;
        world->Step(timeStep, 6, 2);

        if (addedEntities.size())
        {
            for (auto &addedEntiy : addedEntities)
            {
                entities.push_back(addedEntiy);
            }

            addedEntities.clear();
        }

        for (auto &entity : entities)
        {
            entity->update(delta);
        }

        if (hasRemoval)
        {
            entities.erase(std::remove_if(entities.begin(), entities.end(), [](const std::shared_ptr<Entity> &e) -> bool { return e->shouldRemove(); }), entities.end());
        }
    }

    void Game::setRemoval()
    {
        hasRemoval = true;
    }

    void Game::addEntity(std::shared_ptr<Entity> entity)
    {
        entity->setId(currentEntityIndex++);
        addedEntities.push_back(entity);
    }

    const std::vector<std::shared_ptr<Entity>> &Game::getEntities() const
    {
        return entities;
    }

    const std::unordered_map<std::string, std::shared_ptr<Texture>> &Game::getTextureMap() const
    {
        return textureMap;
    }

    b2World *Game::getWorld()
    {
        return world.get();
    }

    bool Game::isKeyDown(int keycode)
    {
        return al_key_down(&keyboardState, keycode);
    }

    bool Game::isKeyJustDown(int keycode)
    {
        return al_key_down(&keyboardState, keycode) && !al_key_down(&prevKeyboardState, keycode);
    }

    Game *Game::getInstance()
    {
        return instance;
    }
} // namespace eg
