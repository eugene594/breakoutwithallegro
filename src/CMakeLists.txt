cmake_minimum_required(VERSION 3.16)
project(breakout)

add_subdirectory(engine)
set(MYSOURCES main.cpp)

add_executable(breakout ${MYSOURCES})
target_link_libraries(breakout engine shapes)
