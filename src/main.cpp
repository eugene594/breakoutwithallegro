#include "engine/Engine.h"

int main(int argc, char *argv[])
{
    eg::Engine engine("Breakout", 800, 720);
    engine.run();

    return 0;
}